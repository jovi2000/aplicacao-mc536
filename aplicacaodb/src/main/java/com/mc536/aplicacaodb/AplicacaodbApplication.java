package com.mc536.aplicacaodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplicacaodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplicacaodbApplication.class, args);
	}

}