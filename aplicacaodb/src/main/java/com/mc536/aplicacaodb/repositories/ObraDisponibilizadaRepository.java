package com.mc536.aplicacaodb.repositories;

import com.mc536.aplicacaodb.dto.EstudanteDTO;
import com.mc536.aplicacaodb.dto.ObrasMaisDisponibilizadasDTO;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ObraDisponibilizadaRepository {
    @Query(value = "WITH\n" +
            "\n" +
            "id_obra_disponivel AS (\n" +
            "  \tSELECT\n" +
            "  \n" +
            "\tid_obra,\n" +
            "\tCOUNT(*) AS disponibilizacoes\n" +
            "\n" +
            "\tFROM obraDisponibilizada\n" +
            "\tGROUP BY id_obra\n" +
            ")\n" +
            "\n" +
            "SELECT \n" +
            " \n" +
            "b.nome,\n" +
            "b.genero,\n" +
            "b.autor,\n" +
            "b.data_publicacao,\n" +
            "a.disponibilizacoes\n" +
            " \n" +
            "FROM id_obra_disponivel a\n" +
            "INNER JOIN obraLiteraria b ON a.id_obra = b.id_obra\n" +
            "ORDER BY disponibilizacoes DESC;", nativeQuery = true)
    List<ObrasMaisDisponibilizadasDTO> findObrasMaisDisponibilizadas();

//    List<EstudanteDTO> findAlunosPorNome();
}
