package com.mc536.aplicacaodb.controllers;

import com.mc536.aplicacaodb.dto.EstudanteDTO;
import com.mc536.aplicacaodb.dto.LeitoresDisponibilizadosEmCadaRegiaoDTO;
import com.mc536.aplicacaodb.dto.ObrasMaisDisponibilizadasDTO;
import com.mc536.aplicacaodb.models.Estado;
import com.mc536.aplicacaodb.repositories.LeitorDigitalRepository;
import com.mc536.aplicacaodb.repositories.ObraDisponibilizadaRepository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/obra_disponibilizada")
public class ObraDisponibilizadaController {
    @Autowired
    private ObraDisponibilizadaRepository obraDisponibilizadaRepository;

    @GetMapping("/obras_mais_disponibilizadas")
    public List<ObrasMaisDisponibilizadasDTO> findObrasMaisDisponibilizadas() {
        return obraDisponibilizadaRepository.findObrasMaisDisponibilizadas();
    }

//    @GetMapping("/aluno/{nome}")
//    public List<EstudanteDTO> findAlunosPorNome(@PathVariable(value = "nome") long id) {
//        List<EstudanteDTO> estudantes = obraDisponibilizadaRepository.findAlunosPorNome(nome);
//
//        if(estudantes.isEmpty()) {
//            return ResponseEntity.notFound().build();
//        } else {
//            return ResponseEntity.ok().body(estudantes);
//        }
//    }
}
