package com.mc536.aplicacaodb.controllers;

import com.mc536.aplicacaodb.models.Estado;
import com.mc536.aplicacaodb.repositories.EstadoRepository;

import java.util.List;
import java.util.Optional;

import org.hibernate.annotations.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/estado")
public class EstadoController {

    @Autowired
    private EstadoRepository estadoRepository;

    @GetMapping
    public List<Estado> findAllEstados() {
        return  (List<Estado>) estadoRepository.findAll();
    }
    @GetMapping("/teste")
    public List<Long> findTeste() {
        return estadoRepository.teste();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Estado> findEstadoById(@PathVariable(value = "id") long id) {
        Optional<Estado> estado = estadoRepository.findById(id);

        if(estado.isPresent()) {
            return ResponseEntity.ok().body(estado.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
