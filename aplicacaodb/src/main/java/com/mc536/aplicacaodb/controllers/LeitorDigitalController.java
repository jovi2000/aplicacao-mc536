package com.mc536.aplicacaodb.controllers;


import com.mc536.aplicacaodb.dto.LeitoresDisponibilizadosEmCadaRegiaoDTO;
import com.mc536.aplicacaodb.repositories.LeitorDigitalRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/leitor")
public class LeitorDigitalController {

    @Autowired
    private LeitorDigitalRepository leitorDigitalRepository;

    @GetMapping("/leitores_regiao")
    public List<LeitoresDisponibilizadosEmCadaRegiaoDTO> findLeitoresDisponibilizadosEmCadaRegiao() {
        return leitorDigitalRepository.findLeitoresDisponibilizadosEmCadaRegiao();
    }
}