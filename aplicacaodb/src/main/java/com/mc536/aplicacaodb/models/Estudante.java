package com.mc536.aplicacaodb.models;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "estudante")
public class Estudante {
    @Id
    private long id_estudante;
    private String nome;
    private String cpf;
    private Date data_nascimento;
    private Sexo sexo;
    private long id_escola;

    public long getId_estudante() {
        return id_estudante;
    }

    public void setId_estudante(long id_estudante) {
        this.id_estudante = id_estudante;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getData_nascimento() {
        return data_nascimento;
    }

    public void setData_nascimento(Date data_nascimento) {
        this.data_nascimento = data_nascimento;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setId_cidade(Sexo sexo) {
        this.sexo = sexo;
    }

    public long getId_escola() {
        return id_escola;
    }

    public void setId_escola(long id_escola) {
        this.id_escola = id_escola;
    }
}
