package com.mc536.aplicacaodb.dto;

public class EstudanteDTO {
    private String nome_estudante;
    private long id_leitor;
    private long id_obra;
    private String nome_obra;
    private String nome_autor;
}
