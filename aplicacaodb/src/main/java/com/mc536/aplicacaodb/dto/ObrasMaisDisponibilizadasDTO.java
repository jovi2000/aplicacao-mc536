package com.mc536.aplicacaodb.dto;

import java.sql.Date;

public class ObrasMaisDisponibilizadasDTO {
    private String nome;
    private String genero;
    private String autor;
    private Date data_publicacao;
    private long disponibilizacoes;
}
